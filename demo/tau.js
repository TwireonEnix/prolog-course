const pl = require('tau-prolog');
const R = require('ramda');
const session = pl.create();

const program = `
parent(pam, bob).
parent(tom, bob).
parent(tom, liz).
parent(bob, ann).
parent(bob, patty).
parent(patty, jim).
parent(bob, patty).
parent(patty, jim).

female(pam).
female(patty).
female(ann).
female(liz).
male(tom).
male(bob).
male(jim).

sister(X, Y) :- parent(Z, X), parent(Z, Y), female(X), X \\== Y.
mother(X,Y) :- parent(X, Y), female(X).
father(X, Y) :- parent(X, Y), male(X).
hasChild(X) :- parent(X, _).
brother(X, Y) :- parent(Z, X), parent(Z, Y), male(X), X \\== Y.
isMother(X) :- mother(X, _).
isFather(Y) :- father(Y, _).
grandmother(X,Y) :- mother(X, Z), parent(Z, Y).
grandparent(X,Y) :- parent(X, Z), parent(Z, Y).
wife(X, Y) :- father(Y, Z), mother(X, Z).
uncle(X, Y) :- brother(X, Z), parent(Z, Y).
aunt(X, Y) :- sister(X, Z), parent(Z, Y).
husband(X, Y) :- wife(Y, X).

predecessor(X, Z) :- parent(X, Z), !.
predecessor(X, Z) :- parent(X, Y), predecessor(Y, Z).

factorial(1,1).
factorial(N, R) :- N>1, N1 is N - 1, factorial(N1, R1), R is N*R1.`;

session.consult(program);

module.exports = queryStr => {
	session.query(queryStr);
	function extractAnswersImpure(...res) {
		const addToResults = x => res.push(pl.format_answer(x));
		do {
			session.answer(addToResults);
		} while (!R.includes('false', R.last(res)));
		return [...res];
	}
	const results = extractAnswersImpure();
	return results.length > 1 ? R.init(results) : results;
};
